import numpy as np


def iteration_matrix(lower, central, upper, size):
    d0 = lower * np.ones(size - 1)
    d1 = central * np.ones(size)
    d2 = upper * np.ones(size - 1)
    return np.diag(d1) + np.diag(d0, k=1) + np.diag(d2, k=-1)


def steady_state(f, dx, mu, u_left, u_right):
    A = (mu / (dx ** 2)) * iteration_matrix(-1, 2, -1, len(f))

    rhs = np.copy(f)
    rhs[0] = rhs[0] + (mu / (dx ** 2)) * u_left
    rhs[len(rhs) - 1] = rhs[len(rhs) - 1] + (mu / (dx ** 2)) * u_right

    return np.linalg.solve(A, rhs)


def explicit_scheme(u, f, dt, dx, mu, u_left, u_right):
    u_new = np.copy(u)

    u_new[0] = ((dt * mu) / (dx ** 2)) * (u[1] - 2 * u[0] + u_left) + u[0] + f[0] * dt

    for i in range(1, len(u_new) - 1):
        u_new[i] = ((dt * mu) / (dx ** 2)) * (u[i + 1] - 2 * u[i] + u[i - 1]) + u[i] + f[i] * dt

    u_new[len(u_new) - 1] = (((dt * mu) / (dx ** 2)) * (u_right - 2 * u[len(u_new) - 1] + u[len(u_new) - 2]) + u[len(u_new) - 1] + f[len(u_new) - 1] * dt)

    return u_new


def implicit_scheme(u, f, dt, dx, mu, u_left, u_right):
    A = iteration_matrix(((-mu * dt) / (dx ** 2)), (1 + 2 * (mu * dt) / (dx ** 2)), ((-mu * dt) / (dx ** 2)), len(u))

    rhs = np.copy(u)
    for i in range(len(rhs)):
        if i == 0:
            rhs[i] = rhs[i] + f[i] * dt + ((mu * dt) / (dx ** 2)) * u_left
        elif i == (len(rhs) - 1):
            rhs[i] = rhs[i] + f[i] * dt + ((mu * dt) / (dx ** 2)) * u_right
        else:
            rhs[i] = rhs[i] + f[i] * dt

    return np.linalg.solve(A, rhs)


def crank_nicholson_scheme(u, f, dt, dx, mu, u_left_0, u_right_0, u_prim_left_1, u_prim_right_1):
    gamma = (mu * dt) / (2 * (dx ** 2))

    A = iteration_matrix(-gamma, 1 + 2 * gamma, -gamma, len(u))
    B = iteration_matrix(gamma, 1 - 2 * gamma, gamma, len(u))

    rhs = np.copy(u)
    for i in range(len(rhs)):
        if i == 0:
            rhs[i] = rhs[i] + f[i] * dt + gamma * u_left_0 + gamma * (u_prim_left_1 * dt)
        elif i == (len(rhs) - 1):
            rhs[i] = rhs[i] + f[i] * dt + gamma * u_right_0 + gamma * (u_prim_right_1 * dt)
        else:
            rhs[i] = rhs[i] + f[i] * dt

    return np.dot(np.linalg.inv(A), np.dot(B, u) + rhs)
