import numpy as np
import math


def f(value, flag):
    if flag == "A":
        return value * (1 - value), np.min(value * (1 - value)), np.max(value * (1 - value))
    elif flag == "B":
        return np.exp(-500 * ((value - 0.5) ** 2)), np.min(np.exp(-500 * ((value - 0.5) ** 2))), np.max(np.exp(-500 * ((value - 0.5) ** 2)))
    elif flag == "C":
        vect = np.array([])
        for v in value:
            if 1/3 <= v <= 2/3:
                vect = np.append(vect, 1)
            else:
                vect = np.append(vect, 0)
        return vect, np.min(vect), np.max(vect)
    elif flag == "D":
        return np.sin(16 * math.pi * value), np.min(np.sin(16 * math.pi * value)), np.max(np.sin(16 * math.pi * value))
    elif flag == "E":
        return np.sin(2 * math.pi * value), np.min(np.sin(2 * math.pi * value)), np.max(np.sin(2 * math.pi * value))
    elif flag == "F":
        return np.sin(4 * math.pi * value) + 0.2 * np.sin(40 * math.pi * value), np.min(np.sin(4 * math.pi * value) + 0.2 * np.sin(40 * math.pi * value)), np.max(np.sin(4 * math.pi * value) + 0.2 * np.sin(40 * math.pi * value))
