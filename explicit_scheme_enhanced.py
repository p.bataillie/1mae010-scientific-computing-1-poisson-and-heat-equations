import numpy as np
import matplotlib.pyplot as plt
import numerical_methods
import initial_distribution
import figure


def explicit_scheme(ax, distribution, cfl):
    x_left = 0
    x_right = 1
    u_left = 0
    u_right = 0

    N = 10
    mu = 1

    dx = (x_right - x_left) / (N + 1)
    x = (np.arange(N) + 1) * dx

    dt = (cfl * (dx ** 2.0)) / (2 * mu)

    u_init, y_min, y_max = initial_distribution.f(x, distribution)

    y_min = y_min - 0.1
    y_max = y_max + 0.1

    t = 0
    t_max = 0.2

    u = u_init
    u_new = np.copy(u)

    ax.grid()
    ax.set(xlabel='x')
    ax.set(ylabel='u')
    ax.set(xlim=[x_left, x_right])
    ax.set(ylim=[y_min, y_max])

    xx = np.copy(x)
    xx = np.insert(xx, 0, 0.0)
    xx = np.insert(xx, len(xx), dx * (N + 1))

    uu = np.copy(u_new)
    uu = np.insert(uu, 0, u_left)
    uu = np.insert(uu, len(uu), u_right)

    ax.plot(xx, uu)
    plt.draw()

    plt.pause(0.1)

    while t < t_max:
        t = t + dt

        u_new = numerical_methods.explicit_scheme(u, np.zeros(np.size(u)), dt, dx, mu, u_left, u_right)
        u = np.copy(u_new)

        ax.clear()
        ax.grid()
        ax.set(xlabel='x')
        ax.set(ylabel='u')
        ax.set(xlim=[x_left, x_right])
        ax.set(ylim=[y_min, y_max])

        xx = np.copy(x)
        xx = np.insert(xx, 0, 0.0)
        xx = np.insert(xx, len(xx), dx * (N + 1))

        uu = np.copy(u_new)
        uu = np.insert(uu, 0, u_left)
        uu = np.insert(uu, len(uu), u_right)

        ax.plot(xx, uu)
        plt.draw()

        plt.pause(0.01)

    figure.max_value(u_new, x)


cfl = 0.9
fig, axs = plt.subplots(2, 3)
fig.suptitle('Solution of u(t,x) with Explicit Scheme')
explicit_scheme(axs[0, 0], 'A', cfl)
explicit_scheme(axs[0, 1], 'B', cfl)
explicit_scheme(axs[0, 2], 'C', cfl)
explicit_scheme(axs[1, 0], 'D', cfl)
explicit_scheme(axs[1, 1], 'E', cfl)
explicit_scheme(axs[1, 2], 'F', cfl)
plt.show(block=True)
