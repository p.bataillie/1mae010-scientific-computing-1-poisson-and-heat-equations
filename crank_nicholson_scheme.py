import numpy as np
import matplotlib.pyplot as plt
import numerical_methods
import initial_distribution
import figure


def crank_nicholson_scheme(distribution, cfl):
    title = 'Solution of u(t,x) with Crank Nicholson Scheme'
    x_label = 'x'
    y_label = 'u'

    x_left = 0
    x_right = 1
    u_left = 0
    u_right = 0

    N = 10
    mu = 1

    dx = (x_right - x_left) / (N + 1)
    x = (np.arange(N) + 1) * dx

    dt = (cfl * (dx ** 2.0)) / (2 * mu)

    u_init, u_min, u_max = initial_distribution.f(x, distribution)

    u_min = u_min - 0.1
    u_max = u_max + 0.1

    t = 0
    t_max = 0.2

    u = u_init
    u_new = np.copy(u)

    figure.plot(x, u_new, u_left, u_right, u_min, u_max, x_left, x_right, x_label, y_label, title)

    while t < t_max:
        t = t + dt

        u_new = numerical_methods.crank_nicholson_scheme(u, np.zeros(np.size(u)), dt, dx, mu, u_left, u_right, 0, 0)
        u = np.copy(u_new)

        figure.plot(x, u_new, u_left, u_right, u_min, u_max, x_left, x_right, x_label, y_label, title)

    figure.max_value(u_new, x)


fig = plt.figure()
crank_nicholson_scheme('A', 0.4)
plt.show(block=True)
