import numpy as np
import timeit
import numerical_methods
import initial_distribution


def explicit_scheme_computational_time(distribution, cfl):
    x_left = 0
    x_right = 1
    u_left = 0
    u_right = 0

    N = 10
    mu = 1

    dx = (x_right - x_left) / (N + 1)
    x = (np.arange(N) + 1) * dx

    dt = (cfl * (dx ** 2.0)) / (2 * mu)

    u_init, u_min, u_max = initial_distribution.f(x, distribution)

    t = 0
    t_max = 0.2

    tic = timeit.default_timer()

    u = u_init
    u_new = np.copy(u)

    while t < t_max:
        t = t + dt

        u_new = numerical_methods.explicit_scheme(u, np.zeros(np.size(u)), dt, dx, mu, u_left, u_right)
        u = np.copy(u_new)

    toc = timeit.default_timer()

    print("Explicit Scheme | The computational time is: " + str(toc - tic) + ' s.')
    return toc - tic


def implicit_scheme_computational_time(distribution, cfl):
    x_left = 0
    x_right = 1
    u_left = 0
    u_right = 0

    N = 10
    mu = 1

    dx = (x_right - x_left) / (N + 1)
    x = (np.arange(N) + 1) * dx

    dt = (cfl * (dx ** 2.0)) / (2 * mu)

    u_init, u_min, u_max = initial_distribution.f(x, distribution)

    t = 0
    t_max = 0.2

    tic = timeit.default_timer()

    u = u_init
    u_new = np.copy(u)

    while t < t_max:
        t = t + dt

        u_new = numerical_methods.implicit_scheme(u, np.zeros(np.size(u)), dt, dx, mu, u_left, u_right)
        u = np.copy(u_new)

    toc = timeit.default_timer()

    print("Implicit Scheme | The computational time is: " + str(toc - tic) + ' s.')
    return toc - tic


time_explicit = explicit_scheme_computational_time('A', 0.9)
time_implicit = implicit_scheme_computational_time('A', 0.9)
print('The Explicit Scheme is ' + str(time_implicit/time_explicit) + ' times faster than the Implicit Scheme.')
