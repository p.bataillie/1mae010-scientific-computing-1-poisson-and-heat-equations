import numpy as np
import matplotlib.pyplot as plt

import figure
import numerical_methods


def f(value, time, mu):
    if isinstance(value, np.ndarray):
        res = np.array([])
        for v in value:
            res = np.append(res, (v * (1 - v)) + (2 * mu * time))
        return res


def u_exact(value, time):
    return value * (1 - value) * time


def discretisation_error(cfl, scheme):
    title = 'Solution of u(t,x) with ' + scheme + ' Scheme'
    x_label = 'x'
    y_label = 'Numerical Error'

    x_left = 0
    x_right = 1
    u_left = 0
    u_right = 0

    N = 10
    mu = 1

    dx = (x_right - x_left) / (N + 1)
    x = (np.arange(N) + 1) * dx

    dt = (cfl * (dx ** 2.0)) / (2 * mu)

    t = 0
    t_max = 0.2

    u_init = u_exact(x, t)

    u_min = np.min(u_init) - 0.1
    u_max = np.max(u_init) + 0.1

    u = u_init
    u_new = np.copy(u)

    figure.plot_discretisation_error(x, u_new, u_exact(x, t), u_left, u_right, u_min, u_max, x_left, x_right,
                                     x_label, y_label, title)

    while t < t_max:

        t = t + dt

        f_source = f(x, t, mu)

        if scheme.lower() == "implicit":
            u_new = numerical_methods.implicit_scheme(u, f_source, dt, dx, mu, u_left, u_right)
        elif scheme.lower() == "explicit":
            u_new = numerical_methods.explicit_scheme(u, f_source, dt, dx, mu, u_left, u_right)
        else:
            print("ERROR: Missing Scheme Specification!")

        u = np.copy(u_new)

        figure.plot_discretisation_error(x, u_new, u_exact(x, t), u_left, u_right, u_min, u_max, x_left, x_right,
                                         x_label, y_label, title)

    maxVal = max(u_new)
    po = np.where(u_new == maxVal)
    print("The max value is " + str(maxVal) + " at the point (indexed by " + str(po[0]) + ") = " + str(x[po[0]]))


plt.figure()
discretisation_error(0.9, 'Explicit')

plt.figure()
discretisation_error(0.9, 'Implicit')

plt.show(block=True)
