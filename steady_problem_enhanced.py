import numpy as np
import matplotlib.pyplot as plt
import numerical_methods


def f(value):
    return value**4 + 3*value + 1


def q1(N):
    x_left = 0
    x_right = 1
    u_left = 0
    u_right = 0

    mu = 1
    dx = (x_right - x_left) / (N + 1)

    x = (np.arange(N) + 1) * dx

    u = numerical_methods.steady_state(f(x), dx, mu, u_left, u_right)

    xx = np.copy(x)
    xx = np.insert(xx, 0, 0.0)
    xx = np.insert(xx, len(xx), dx * (N+1))

    uu = np.copy(u)
    uu = np.insert(uu, 0, u_left)
    uu = np.insert(uu, len(uu), u_right)

    u_exact = - (1/mu) * ((xx**6)/30 + (1/2)*(xx**3) + (1/2)*(xx**2) - (31/30)*xx)

    error = max(np.abs(u_exact - uu))
    return dx, error


log_dx = []
log_error = []
node_number = [10, 20, 30]

for node in node_number:
    dx, error = q1(node)
    log_dx.append(np.log(dx))
    log_error.append(np.log(error))

plt.figure()
plt.xlabel('log(dx)')
plt.ylabel('log(error)')
plt.plot(log_dx, log_error)
plt.grid()
plt.title('Numerical Error according to Spatial Discretisation - Steady State Problem')
plt.show()

# We can see that the error is quadratic with respect to dx: the slope is 2.
