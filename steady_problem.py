import numpy as np
import matplotlib.pyplot as plt
import numerical_methods
import figure


def f(value):
    return value**4 + 3*value + 1


x_left = 0
x_right = 1
u_left = 0
u_right = 0
t_max = 0.2
N = 10
mu = 1
dx = (x_right - x_left) / (N + 1)

x = (np.arange(N) + 1) * dx

u = numerical_methods.steady_state(f(x), dx, mu, u_left, u_right)

xx = np.copy(x)
xx = np.insert(xx, 0, 0.0)
xx = np.insert(xx, len(xx), dx * (N+1))

uu = np.copy(u)
uu = np.insert(uu, 0, u_left)
uu = np.insert(uu, len(uu), u_right)

plt.figure()
plt.plot(xx, uu)
plt.grid()
plt.xlabel('x')
plt.ylabel('u')
plt.title('Approximate Solution of u(x) - Steady State Problem')
plt.show()

figure.max_value(u, x)
