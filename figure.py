import numpy as np
import matplotlib.pyplot as plt
from IPython import display


def max_value(u_new, x):
    max_val = np.max(u_new)
    po = np.where(u_new == max_val)
    print("The max value is " + str(max_val) + " at the point (indexed by " + str(po[0]) + ") = " + str(x[po[0]]))


def plot(x, u, u_left, u_right, u_min, u_max, x_left, x_right, x_label, y_label, title, notebook=False):
    xx = np.copy(x)
    xx = np.insert(xx, 0, x_left)
    xx = np.insert(xx, len(xx), x_right)

    uu = np.copy(u)
    uu = np.insert(uu, 0, u_left)
    uu = np.insert(uu, len(uu), u_right)

    plt.clf()
    plt.grid()
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.xlim([x_left, x_right])
    plt.ylim([u_min, u_max])
    plt.title(title)
    plt.plot(xx, uu)
    plt.draw()
    if notebook:
        display.clear_output(wait=True)
    plt.pause(0.01)


def plot_discretisation_error(x, u, u_exact, u_left, u_right, u_min, u_max, x_left, x_right, x_label, y_label, title, notebook=False):
    xx = np.copy(x)
    xx = np.insert(xx, 0, x_left)
    xx = np.insert(xx, len(xx), x_right)

    uu = np.copy(u)
    uu = np.insert(uu, 0, u_left)
    uu = np.insert(uu, len(uu), u_right)

    u_ex = np.copy(u_exact)
    uu_ex = np.copy(u_ex)
    uu_ex = np.insert(uu_ex, 0, u_left)
    uu_ex = np.insert(uu_ex, len(uu_ex), u_right)

    plt.clf()
    plt.grid()
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.xlim([x_left, x_right])
    plt.plot(xx, np.abs(uu_ex - uu))
    plt.draw()
    if notebook:
        display.clear_output(wait=True)
    plt.pause(0.01)


def plot_discretisation_error_compare(x_explicit, e_explicit, x_implicit, e_implicit, e_left, e_right, e_min, e_max, x_left, x_right, x_label, y_label, title):
    xx_explicit = np.copy(x_explicit)
    xx_explicit = np.insert(xx_explicit, 0, x_left)
    xx_explicit = np.insert(xx_explicit, len(xx_explicit), x_right)

    ee_explicit = np.copy(e_explicit)
    ee_explicit = np.insert(ee_explicit, 0, e_left)
    ee_explicit = np.insert(ee_explicit, len(ee_explicit), e_right)

    xx_implicit = np.copy(x_implicit)
    xx_implicit = np.insert(xx_implicit, 0, x_left)
    xx_implicit = np.insert(xx_implicit, len(xx_implicit), x_right)

    ee_implicit = np.copy(e_implicit)
    ee_implicit = np.insert(ee_implicit, 0, e_left)
    ee_implicit = np.insert(ee_implicit, len(ee_implicit), e_right)

    plt.clf()
    plt.grid()
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.xlim([x_left, x_right])
    plt.ylim([e_min, e_max])

    plt.plot(xx_explicit, ee_explicit, label='Explicit Scheme')
    plt.plot(xx_implicit, ee_implicit, 'r--', label='Implicit Scheme')

    plt.legend()
    plt.draw()
